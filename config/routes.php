<?php

$pathInfo = $_SERVER['QUERY_STRING'];

$path = explode('/', $pathInfo);

if(!isset($path[1])){
  $path[1]  = 'index';
}
//echo $pathInfo;
//echo '<pre>';
//var_dump($_SERVER);echo '</pre>';die;

$routes = [ 
  'index' => [
    'controller' => 'postsController',
    'method' => 'index'
  ],
  'post' => [
    'controller' => 'postsController',
    'method' => 'show',
    'param' => 1
  ],
  
  'login' => [
    'controller' => 'loginController',
    'method' => 'login'
  ],
  'dashboard' => [
    'controller' => 'loginController',
    'method' => 'dashboard'
  ],
  'logout' => [
    'controller' => 'loginController',
    'method' => 'logout'
  ],
  'posts-admin' => [
    'controller' => 'postsAdminController',
    'method' => 'index'
  ],
  'posts-edit' => [
    'controller' => 'postsAdminController',
    'method' => 'edit',
    'param' => 1
  ],
];

if (isset($routes[$path[1]])) {
  $route = $routes[$path[1]];
  $controller = $container->make($route['controller']);
  $method = $route['method'];
  if(isset($route['param'])){
    $param = $path[2];    
    $controller->$method($param);
  }else{
    $controller->$method();  
  }
  
}


/*
if ($pathInfo == "/index") {
  $postsController = $container->make("postsController");
  $postsController->index();
} elseif ($pathInfo == "/post") {
  $postsController = $container->make("postsController");
  $postsController->show();
}
*/


?>
<?php

  session_start();

  require __DIR__ . "/init.php";

  require __DIR__ . "/config/config.php";

  if(isset($config['helper'])){
	  foreach($config['helper'] as $helper){
	  	require __DIR__ . "/helpers/" . $helper . "_helper.php";
	  }    	
  }


  require __DIR__ . "/config/routes.php";



?>
